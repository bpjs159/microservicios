### Microservicios###
Ejercicio: Crear una arquitectura orientada a los microservicios, implementando una calculadora donde las operaciones de suma, resta, multiplicación y división son servicios consumidos por un API Gateway, que a su vez es consumida por un cliente.


### Solución ###

Se ha propuesto utilizar python para dar solucion a una calculadora, para ello se ha utilizado entornos virtuales para poder separar las logicas y los paquetes de los microservicios. y Flask para emitir una API para ser consumida en cada uno de ellos. 

### Pruebas ###

/arquitectura.png	Esta imagen muestra a la izquierda los cuatro micro servicios suma,resta, multiplicacion y division. y a la derecha tenemos el API Gateway y el cliente. 

/suma.png	Esta imagen muestra como el cliente hace la suma de 2 + 2 y podemos ver en las consolas como el API Gateway tiene una petición y el micro servicio suma tiene otra.

/multiplicacion.png	Esta imagen muestra como el cliente hace la multiplicacion de 4 * 3 y podemos ver en las consolas como el API Gateway tiene otra petición y el micro servicio multiplicacion tiene otra.


### Integrantes ###

* 20141020053 Johan Sebastian Bonilla Plata
* 20141020096 Javier Duvan Hospital Melo
* 20141020135 Miguel Ángel Hernández Cubillos


