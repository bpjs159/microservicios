import requests 
from flask import Flask

#haciendo uso de API para suma, resta multiplicacion y division

puertos = ["8081", "8082", "8083", "8084"] #0suma #1resta #2multiplicacion #3division

def calcular(ope,x,y):
    return requests.get(url = "http://localhost:"+puertos[int(ope)]+"/"+str(x)+"/"+str(y)).text
 

#Exponiendo servicio al cliente oficial

app = Flask(__name__)

@app.route('/<ope>/<x>/<y>')
def show_user_profile(ope,x,y):
    return calcular(ope,x,y)

if __name__ == '__main__':
    app.run(port=8080)