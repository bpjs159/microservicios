from flask import Flask
app = Flask(__name__)

@app.route('/<x>/<y>')
def show_user_profile(x,y):
    return str(float(x)+float(y))

if __name__ == '__main__':
    app.run(port=8081)