from flask import Flask
app = Flask(__name__)

@app.route('/<x>/<y>')
def show_user_profile(x,y):
    if y!="0":
        return str(float(x)/float(y))
    else:
        return "ERROR"

if __name__ == '__main__':
    app.run(port=8084)